package events

var (
	SCP_1 = NewEvent(
		"scp_1",
		"started listening",
		Low,
	)
	SCP_2 = NewEvent(
		"scp_2",
		"server closed",
		High,
	)
	SCP_3 = NewEvent(
		"scp_3",
		"received request for app manager",
		Low,
	)
	SCP_4 = NewEvent(
		"scp_4",
		"sent response with app manager",
		Low,
	)
	SCP_5 = NewEvent(
		"scp_5",
		"failed to fetch app manager",
		High,
	)
	SCP_6 = NewEvent(
		"scp_6",
		"failed to encode response payload",
		High,
	)
	SCP_7 = NewEvent(
		"scp_7",
		"received request for openapi spec as JSON",
		Low,
	)
	SCP_8 = NewEvent(
		"scp_8",
		"sent response with openapi spec as JSON",
		Low,
	)
	SCP_9 = NewEvent(
		"scp_9",
		"received request for openapi spec as YAML",
		Low,
	)
	SCP_10 = NewEvent(
		"scp_10",
		"sent response with openapi spec as YAML",
		Low,
	)
	SCP_11 = NewEvent(
		"scp_11",
		"failed to read openapi.json file",
		High,
	)
	SCP_12 = NewEvent(
		"scp_12",
		"failed to write fileBytes to response",
		High,
	)
	SCP_13 = NewEvent(
		"scp_13",
		"failed to read openapi.yaml file",
		High,
	)
	SCP_14 = NewEvent(
		"scp_14",
		"received request for organizations list",
		Low,
	)
	SCP_15 = NewEvent(
		"scp_15",
		"sent response with organizations list",
		Low,
	)
	SCP_16 = NewEvent(
		"scp_16",
		"received request to register organization",
		Low,
	)
	SCP_17 = NewEvent(
		"scp_17",
		"sent response with registered organization",
		Low,
	)
	SCP_18 = NewEvent(
		"scp_18",
		"received request for organization",
		Low,
	)
	SCP_19 = NewEvent(
		"scp_19",
		"sent response with organization",
		Low,
	)
	SCP_20 = NewEvent(
		"scp_20",
		"failed to fetch organizations list",
		High,
	)
	SCP_21 = NewEvent(
		"scp_21",
		"failed to decode request payload",
		High,
	)
	SCP_22 = NewEvent(
		"scp_22",
		"failed to fetch organization",
		High,
	)
	SCP_23 = NewEvent(
		"scp_23",
		"organization does not exist",
		High,
	)
	SCP_24 = NewEvent(
		"scp_24",
		"received request for scheme",
		Low,
	)
	SCP_25 = NewEvent(
		"scp_25",
		"sent response with scheme",
		Low,
	)
	SCP_26 = NewEvent(
		"scp_26",
		"failed to fetch scheme",
		High,
	)
	SCP_27 = NewEvent(
		"scp_27",
		"fetched organizations list",
		Low,
	)
	SCP_28 = NewEvent(
		"scp_28",
		"failed to create organization",
		High,
	)
	SCP_29 = NewEvent(
		"scp_29",
		"fetched document types",
		Low,
	)
	SCP_30 = NewEvent(
		"scp_30",
		"fetched app manager list",
		Low,
	)
	SCP_31 = NewEvent(
		"scp_31",
		"failed to decode public key from pem",
		VeryHigh,
	)
)
