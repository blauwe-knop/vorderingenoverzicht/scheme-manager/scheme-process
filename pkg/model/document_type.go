// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type DocumentType struct {
	Name string `json:"name"`
}
