// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Organization struct {
	Oin          string `json:"oin"`
	Name         string `json:"name"`
	DiscoveryUrl string `json:"discoveryUrl"`
	PublicKey    string `json:"publicKey"`
	LogoUrl      string `json:"logoUrl"`
}
