FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/scheme-process/api
COPY ./cmd /go/src/scheme-process/cmd
COPY ./internal /go/src/scheme-process/internal
COPY ./pkg /go/src/scheme-process/pkg
COPY ./go.mod /go/src/scheme-process/
COPY ./go.sum /go/src/scheme-process/
WORKDIR /go/src/scheme-process
RUN go mod download \
 && go build -o dist/bin/scheme-process ./cmd/scheme-process

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/scheme-process/dist/bin/scheme-process /usr/local/bin/scheme-process
COPY --from=build /go/src/scheme-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/scheme-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/scheme-process"]
