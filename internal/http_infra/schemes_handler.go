// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/events"
)

func handlerFetchScheme(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SCP_24
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	scheme, err := schemeUseCase.FetchScheme()
	if err != nil {
		event := events.SCP_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*scheme)
	if err != nil {
		event := events.SCP_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SCP_25
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
