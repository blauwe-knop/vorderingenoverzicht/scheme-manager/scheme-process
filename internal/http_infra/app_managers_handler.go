// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"
)

func handlerFetchAppManager(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	schemeUseCase, _ := context.Value(schemeUseCaseKey).(*usecases.SchemeUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SCP_3
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	appManager, err := schemeUseCase.FetchAppManager(chi.URLParam(request, "oin"))

	if errors.Is(err, repositories.ErrAppManagerNotFound) {
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}

	if err != nil {
		event = events.SCP_5
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*appManager)
	if err != nil {
		event = events.SCP_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SCP_4
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
