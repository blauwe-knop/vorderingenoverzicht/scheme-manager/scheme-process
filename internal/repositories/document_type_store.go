// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type DocumentTypeStore struct {
}

func NewDocumentTypeStore() *DocumentTypeStore {
	return &DocumentTypeStore{}
}

func (s *DocumentTypeStore) GetDocumentTypes() []model.DocumentType {
	documenttypes := []model.DocumentType{
		{
			Name: "DebtRequest",
		},
		{
			Name: "DebtResponse",
		}}

	return documenttypes
}
func (s *DocumentTypeStore) GetHealthCheck() healthcheck.Result {
	return healthcheck.Result{
		Name:         "document-type-store",
		Status:       healthcheck.StatusOK,
		ResponseTime: 0,
		HealthChecks: []healthcheck.Result{},
	}
}
