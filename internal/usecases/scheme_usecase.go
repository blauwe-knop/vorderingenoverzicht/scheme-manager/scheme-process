// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"go.uber.org/zap"

	servicemodel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
	serviceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
)

type SchemeUseCase struct {
	schemeRepository       serviceRepositories.SchemeRepository
	documentTypeRepository repositories.DocumentTypeRepository
	appManagerRepository   repositories.AppManagerRepository
	Logger                 *zap.Logger
}

func NewSchemeUseCase(logger *zap.Logger, schemeRepository serviceRepositories.SchemeRepository, documentTypeRepository repositories.DocumentTypeRepository, appManagerRepository repositories.AppManagerRepository) *SchemeUseCase {
	return &SchemeUseCase{
		schemeRepository:       schemeRepository,
		documentTypeRepository: documentTypeRepository,
		appManagerRepository:   appManagerRepository,
		Logger:                 logger,
	}
}

func (uc *SchemeUseCase) RegisterOrganization(registerOrganization model.Organization) (*model.Organization, error) {
	_, err := ec.ParsePublicKeyFromPem([]byte(registerOrganization.PublicKey))
	if err != nil {
		event := events.SCP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to decode public key from pem: %v", err)
	}

	organization := servicemodel.Organization{
		Oin:          registerOrganization.Oin,
		Name:         registerOrganization.Name,
		DiscoveryUrl: registerOrganization.DiscoveryUrl,
		PublicKey:    registerOrganization.PublicKey,
		Approved:     false,
		LogoUrl:      registerOrganization.LogoUrl,
	}
	registeredOrganization, err := uc.schemeRepository.CreateOrganization(organization)
	if err != nil {
		event := events.SCP_28
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, err
	}

	returnOrganization := &model.Organization{
		Oin:          registeredOrganization.Oin,
		Name:         registeredOrganization.Name,
		DiscoveryUrl: registeredOrganization.DiscoveryUrl,
		PublicKey:    registeredOrganization.PublicKey,
		LogoUrl:      registeredOrganization.LogoUrl,
	}

	return returnOrganization, err
}

func (uc *SchemeUseCase) ListOrganizations() (*[]servicemodel.Organization, error) {
	organizations, err := uc.schemeRepository.ListOrganizations()

	return organizations, err
}

func (uc *SchemeUseCase) FetchOrganization(oin string) (*servicemodel.Organization, error) {
	organizations, err := uc.schemeRepository.GetOrganization(oin)

	return organizations, err
}

func (uc *SchemeUseCase) UpdateOrganization(oin string, organization servicemodel.Organization) (*servicemodel.Organization, error) {
	responseOrganization, err := uc.schemeRepository.UpdateOrganization(oin, organization)

	return responseOrganization, err
}

func (uc *SchemeUseCase) DeleteOrganization(oin string) (*servicemodel.Organization, error) {
	responseOrganization, err := uc.schemeRepository.DeleteOrganization(oin)

	return responseOrganization, err
}

func (uc *SchemeUseCase) FetchAppManager(oin string) (*model.AppManager, error) {
	appManagers, err := uc.appManagerRepository.GetAppManager(oin)

	return appManagers, err
}

func (uc *SchemeUseCase) FetchScheme() (*model.Scheme, error) {
	organizations, err := uc.schemeRepository.ListOrganizations()
	if err != nil {
		event := events.SCP_20
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, err
	}
	event := events.SCP_27
	uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Int("numberOfOrganizationsFetched", len(*organizations)), zap.Reflect("event", event))

	approvedOrganizations := []servicemodel.Organization{}
	for _, o := range *organizations {
		if o.Approved {
			approvedOrganizations = append(approvedOrganizations, o)
		}
	}

	documentTypes := uc.documentTypeRepository.GetDocumentTypes()
	event = events.SCP_29
	uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	appManagers := uc.appManagerRepository.GetAppManagerList()
	event = events.SCP_30
	uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	scheme := model.Scheme{Organizations: approvedOrganizations, DocumentTypes: documentTypes, AppManagers: appManagers}

	return &scheme, nil
}

func (uc *SchemeUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.schemeRepository,
		uc.documentTypeRepository,
	}
}
